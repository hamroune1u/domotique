import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class TestTelecommande {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testAllume() {
		Lampe l = new Lampe("lampeTest") ;
		l.allumer();
		assertEquals("La lampe devrait �tre allum�e.", l.getAllume(), true);
		
	}
	
	@Test
	public void testEteintLampe() {
		Lampe l = new Lampe("lampeTest");
		l.eteindre();
		assertEquals("La lampe devrait �tre �teinte.", l.getAllume(), false);
	}
	
	@Test
	public void testAllumeHifi() {
		Hifi h = new Hifi();
		h.allumer();
		assertEquals("La cha�ne Hifi devrait �tre allum�e.", h.toString(), ("HIFI:10") );
	}
	
	@Test
	public void testEteintHifi() {
		Hifi h = new Hifi();
		h.eteindre();
		assertEquals("La cha�ne Hifidevrait �tre �teinte.", h.toString(), ("HIFI:0"));
	}

}
