import java.util.*;
public class TelecommandeV4 {

	private ArrayList<AdaptateurV4> controle;
	
	TelecommandeV4(){
		controle=new ArrayList<AdaptateurV4>();
	}
	
	public void activerAppareil(int indice){
		controle.get(indice).activerAppareil();
	}
	
	public void desactiverAppareil(int indice){
		controle.get(indice).desactiverAppareil();
	}
	
	public void ajouterAppareil(AdaptateurV4 l){
		controle.add(l);
	}
	
	
	public String toString(){
		String s="";
		int i=0;
		for (AdaptateurV4 l:controle){
			s+=i+"-"+l+"\n";
			i++;
		}
		return s;
	}

}
