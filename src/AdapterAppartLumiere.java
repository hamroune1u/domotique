import appartement.AppareilAppart;
import appartement.AppareilAppartLampe;

public class AdapterAppartLumiere implements AdaptateurV4 {

	private AppareilAppartLampe aal;
	private String n;
	
	public AdapterAppartLumiere(AppareilAppart aa,String nom) {
		this.aal = (AppareilAppartLampe)aa;
		this.n = nom;
	}
	
	@Override
	public void activerAppareil() {
		// TODO Auto-generated method stub
		this.aal.allumer();
	}

	@Override
	public void desactiverAppareil() {
		// TODO Auto-generated method stub
		this.aal.eteindre();
	}
	
	public String toString() {
		return aal.toString();
	}

}
