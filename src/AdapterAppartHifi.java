import appartement.AppareilAppart;
import appartement.AppareilAppartHifi;


public class AdapterAppartHifi implements AdaptateurV4 {

	private AppareilAppartHifi aah;
	
	public AdapterAppartHifi(AppareilAppart aa) {
		this.aah = (AppareilAppartHifi)aa;
	}
	
	@Override
	public void activerAppareil() {
		// TODO Auto-generated method stub
		this.aah.changerSon(10);
	}

	@Override
	public void desactiverAppareil() {
		// TODO Auto-generated method stub
		this.aah.changerSon(0);
	}
	
	public String toString() {
		return aah.toString();
	}

}
