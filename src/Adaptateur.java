
public class Adaptateur implements Controle {

	private Lumiere l;
	
	public Adaptateur () {
		this.l = new Lumiere();
	}
	
	@Override
	public void allumer() {
		// TODO Auto-generated method stub
		this.l.changerIntensite(10);
	}

	@Override
	public void eteindre() {
		// TODO Auto-generated method stub
		this.l.changerIntensite(0);
	}

}
