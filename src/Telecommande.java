import java.util.*;
public class Telecommande {

	private ArrayList<Controle> controle;
	
	Telecommande(){
		controle=new ArrayList<Controle>();
	}
	
	public void activer(int indice){
		controle.get(indice).allumer();
	}
	
	public void desactiver(int indice){
		controle.get(indice).eteindre();
	}
	
	public void ajouter(Controle l){
		controle.add(l);
	}
	
	public void activerTout(){
		for(Controle l:controle){
			l.allumer();
		}
	}
	
	public String toString(){
		String s="";
		int i=1;
		for (Controle l:controle){
			s+=i+"-"+l;
			i++;
		}
		return s;
	}

}
