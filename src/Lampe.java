
public class Lampe implements Controle {
	
	private String nom;
	private boolean allume;
	
	public Lampe(String nom){
		this.nom=nom;
		this.allume=false;
	}
	
	public void allumer(){
		allume=true;
	}
	
	public void eteindre(){
		allume=false;
	}
	
	public String toString(){
		String s;
		if(allume){
			s="on";
		}else{
			s="off";
		}
		return(this.nom+" : "+s);
	}
	
	public boolean getAllume() {
		return this.allume;
	}
}
