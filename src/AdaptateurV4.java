
public interface AdaptateurV4 {

	public void activerAppareil();
	
	public void desactiverAppareil();
	
	public String toString();
	
}
