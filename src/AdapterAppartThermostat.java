import appartement.AppareilAppart;
import appartement.AppareilAppartThermostat;


public class AdapterAppartThermostat implements AdaptateurV4 {

	private AppareilAppartThermostat aat;
	
	public AdapterAppartThermostat(AppareilAppart aa) {
		this.aat = (AppareilAppartThermostat)aa;
	}
	
	@Override
	public void activerAppareil() {
		// TODO Auto-generated method stub
		this.aat.augmenterTemperature();
	}

	@Override
	public void desactiverAppareil() {
		// TODO Auto-generated method stub
		this.aat.baisserTemperature();
	}
	
	public String toString() {
		return aat.toString();
	}

}
