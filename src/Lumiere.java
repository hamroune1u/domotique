
public class Lumiere {

	private int intensite;
	
	public Lumiere() {
		this.intensite=0;
	}
	
	public void changerIntensite(int i) {
		if(i>=0&&i<=100) {
			this.intensite=i;
		}
	}
	
	public int getLumiere() {
		return this.intensite;
	}
	
	public String toString() {
		return("lumiere " + intensite);
	}
	
}
